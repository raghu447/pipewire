# PipeWire Tutorial

Welcome to the PipeWire tutorial. The goal is to learn to
PipeWire API step-by-step with simple short examples.

1) Getting started [tutorial 1](tutorial1.md).

2) Enumerating objects [tutorial 2](tutorial2.md).
